---
title: ¿De qué tratan las Jornadas de Género y Sofware libre?
date: 2019-05-13T14:57:52.363Z
---
El Software Libre es un movimiento social que brega para que las personas, en comunidad y con acuerdos colectivos, puedan tener el control de los artefactos tecnológicos que utilizan a diario. En consecuencia, este movimiento se centra fuertemente en el aspecto ético y político de los programas o software, elementos que se encuentran en todos los dispositivos electrónicos, y define su posición en la transparencia, igualdad de posibilidades y libre distribución.

Las jornadas de Género y Software libre tienen como fin poder juntar las diferentes comunidades que luchan por la igualdad de posibilidades en las sociedades, para construir conocimientos técnicos y sociales relacionados con la informática desde una mirada de género; para difundir experiencias positivas y buenas prácticas, generar una toma de conciencia y a la vez ofrecer inspiración para que esto se replique en otros contextos; visibilizar los sesgos de género actuales en el mundo laboral y de la construcción de tecnologías -en Software Libre en particular-, producir colectiva y colaborativamente conocimiento libre sobre el tema para que pueda ser compartido; discutir sobre los aportes de igualdad de género en y con las comunidades de Software Libre.

Este año tenemos la consigna "hackeando al patriarcado", donde, además de los temas ya mencionados, interesará discutir el rol de privilegio del “hombre cis” en la construcción de la tecnología y cómo bregar por la igualdad de género.
