---
layout: blog
title: Convocatoria a charlas y talleres en las jornadas
date: 2019-05-13T14:58:33.938Z
---
2ª Jornadas de Género y Software Libre: **Hackeando el patriarcado**.

Fecha: 4 y 5 de Julio de 2019 - Facultad de Ingeniería y Ciencias Hídricas – Universidad Nacional del Litoral.

Estamos convocando a las distintas comunidades relacionadas con género y/o con tecnologías libres a presentar propuestas de charlas, talleres y paneles.

La propuesta debe ser enviada al mail <mailto:tusl@unl.edu.ar> con la siguiente información:

* Tipo de propuesta (charla – taller – participante de panel – otra).
* Título de la propuesta.
* Duración estimada: máximo 40 minutos para charlas – 1 hora para talleres.
* Requisitos técnicos que se van a utilizar (si van a necesitar cañón – pizarrón – pcs para talleres, etc).
* Breve descripción de la propuesta y sus objetivos.
* Temática (programación - género - feminismo - etc).
* Nivel requerido/sugerido de les asistentes (básico o intermedio, sólo para mujeres, etc.).

La _fecha límite_ de presentación es el **viernes 31 de Mayo** de 2019.

Consultas a <mailto:tusl@unl.edu.ar>
